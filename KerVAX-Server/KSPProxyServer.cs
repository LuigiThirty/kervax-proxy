﻿using System;
using System.Net;
using System.Net.Sockets;
using KRPC.Client;
using KRPC.Client.Services.KRPC;

namespace KerVAX_Server
{
    public class KSPProxyServer
    {
        // The TCP server.
        TcpListener listener;

        // The VAX.
        TcpClient client;

        // KSP.
        Connection krpc_connection;

        // The RPC server.
        public Service krpc;

        public KSPProxyServer(IPAddress krpc_ip, UInt16 krpc_port, UInt16 krpc_stream_port,
            IPAddress server_ip, UInt16 server_port)
        {
            listener = new TcpListener(server_ip, server_port);
            krpc_connection = new KRPC.Client.Connection(
                name: "KerVAX",
                address: krpc_ip,
                rpcPort: krpc_port,
                streamPort: krpc_stream_port);
            krpc = krpc_connection.KRPC();
        }

        public bool Start()
        {
            listener.Start();
            return true;
        }

        public bool Stop()
        {
            listener.Stop();
            return true;
        }

        /* Blocking call until a connection happens. */
        public bool WaitForConnection()
        {
            Console.WriteLine("Waiting for a connection...");
            client = listener.AcceptTcpClient();
            Console.WriteLine("Connected!");

            return true;
        }

        public void SendClient(byte[] msg)
        {
            var stream = client.GetStream();
            stream.Write(msg);
        }
    }
}
