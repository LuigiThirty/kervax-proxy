﻿using System;
using System.Net;
using System.Net.Sockets;
using KRPC.Client;
using KRPC.Client.Services.KRPC;

namespace KerVAX_Server
{
    class Program
    {
        static KSPProxyServer proxy_server;

        static void Main(string[] args)
        {
            proxy_server = new KSPProxyServer(IPAddress.Parse("127.0.0.1"),
                50000,
                50001,
                IPAddress.Parse("10.0.0.206"), // TODO: configuration param
                3100);
            proxy_server.Start();


            try
            {
                while (true)
                {
                    // Wait for requests.
                    proxy_server.WaitForConnection();

                    Console.WriteLine("Saying hello to the client.");

                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(
                        String.Format("KerVAX Server: Connected to kRPC version {0}", 
                        proxy_server.krpc.GetStatus().Version));

                    proxy_server.SendClient(msg);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                proxy_server.Stop();
            }

        }
    }
}
